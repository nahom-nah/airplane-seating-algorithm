function Seat(block, row, column, classSeat, passenger) {
    this.block = block;
    this.row = row;
    this.column = column;
    this.classSeat = classSeat;
    this.passenger = passenger;
  }

const main = async () =>{
    let seats = document.getElementById("seats");
    
    document.getElementById("button").addEventListener("click", function() {
        let queue = document.getElementById('queue').value;
        let rowsAndColums = JSON.parse(document.getElementById("rowsAndColums").value);
 
        while(seats.firstChild){
            seats.removeChild(seats.firstChild);
        }
 
        seats.style.background = 'url(\'img/img.png\') no-repeat center top';
        seats.style.backgroundSize = 'contain';

        let result = [];

        sortSeat(rowsAndColums, result);
        result.sort(comparator('column'))
        result.sort(comparator('classSeat'));

        seatPassengers(result, queue)
        
      result.sort(comparator('row', 'column', 'block')); 
      createTableResults(rowsAndColums, result);

    })
}

const comparator=  (key) => {
    return function(a, b) {
      return a[key] - b[key];
    }
  }

const sortSeat = (inputArray, resultArr) => {
    let block, row, column, newSeat;

    for(block = 1; block <= inputArray.length; block++){
      for(column = 1; column <= inputArray[block-1][0]; column++){
        for(row = 1; row <= inputArray[block-1][1]; row++){
          if(block === 1 && column === 1 && inputArray[block-1][0]>1) {
            newSeat = new Seat(block, column, row, 2);
            resultArr.push(newSeat);
          } else if(block === inputArray.length
            && column === inputArray[block-1][0]
            && inputArray[block-1][0]>1){
            newSeat = new Seat(block, column, row, 2);
            resultArr.push(newSeat);
          } else if(column === 1 || column === (inputArray[block-1][0])) {
            newSeat = new Seat(block, column, row, 1);
            resultArr.push(newSeat);
          } else {
            newSeat = new Seat(block, column, row, 3);
            resultArr.push(newSeat);
          }
        }
      }
    }
  }

  const seatPassengers = (res, que) => { 
    if(res.length<que) {
      alert("Only the first "+res.length+" passengers will be able to fly away.");
      for(i=0; i<res.length; i++){
        res[i].passenger = i+1;
        console.log('seat pass 1')
      }
    } else {
      for(i=0; i<que; i++){
        res[i].passenger = i+1;
        console.log('seat pass 2')
      }
    }
  }

 const createTableResults = (arrInput, arrResult)=> {
    let i,z,j,table,tr, td;
    for(i=0; i<arrInput.length; i++){
      table = document.createElement('table');
      table.setAttribute('class', 'table'+(i+1));

      for(j=0; j<arrInput[i][1]; j++) {
        tr = document.createElement('tr');
        tr.setAttribute('class', 'tr'+(j+1));
        for(z=0; z<arrResult.length; z++) {
          if(arrResult[z].block===i+1 && arrResult[z].column===j+1) {
            td = document.createElement('td');
            td.setAttribute('class', 'class'+arrResult[z].classSeat);
            if(isNaN(arrResult[z].passenger)===false) {
              td.innerText=arrResult[z].passenger;
            } else{
              td.innerText="";
            }
            tr.appendChild(td);
          }
        }
        table.appendChild(tr);
      }
      seats.appendChild(table);
    }
  }
 
main().catch(err=>{
    console.log(`something went wrong : ${err}`);
}); 